#!/bin/sh

set -u

if [ -z "$@" ]; then
  exec python3 hashtopolis.zip --url $HCT_SERVER --voucher $HCT_VOUCHER
else
  exec "$@"
fi
