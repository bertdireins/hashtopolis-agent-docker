# hashtopolis-agent-docker

This is a docker image for running a hashtopolis (hashcat) agent in docker for nvidia GPUs, based on CUDA.

## Run

```
nvidia-docker run -e HCT_SERVER=http://<server url>/api/server.php -e HCT_VOUCHER=<voucher> <image>
```

Depending on your task settings you have to make the new agent trusted.

> Jobs get delivered automatically when they have a proiority > 0 and do not use trusted assets/files.

## Check

- Comments in Dockerfile
- When running in vastAI instances have to be destroyed by hand as the process waits for new tasks when finished.
- Inspect benchmarking error on hc6.2.6