# ERROR on hc -b (device 1 out of resources): FROM nvidia/opencl
# Works w hc6.2.6, but not in hashtopolis
FROM nvidia/cuda:11.3.0-devel-ubuntu20.04

# OpenCL stuff - only for nvidia ##########################################################
# Found here: https://github.com/dizcza/docker-hashcat/blob/master/Dockerfile
LABEL com.nvidia.volumes.needed="nvidia_driver"

RUN apt-get update && apt-get install -y --no-install-recommends \
        ocl-icd-libopencl1 \
        intel-opencl-icd \
        clinfo pkg-config && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/OpenCL/vendors && \
    echo "libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
################################ end nvidia opencl driver ################################

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# PROBLEM
# cannot utilise OpenCL for CPU
# - with pocl: Hashcat errors with "old opencl detected"
# - without any ocl: Benchmark errors with 'clGetPlatformIDs(): CL_PLATFORM_NOT_FOUND_KHR' 
#
# Solution
# - Find the right OpenCL drivers for Intel/AMD
# - without any ocl: Igrnore errors by setting ignore errors in hashtopolis config > server to: DeviceGetFanSpeed,clGetPlatformIDs 
#
# These pkgs did not work:
#  pocl-opencl-icd
#  intel-opencl-icd \
#  ocl-icd-opencl-dev
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
RUN apt-get update && apt-get install -y --no-install-recommends \
  zip \
  git \
  python3 \
  python3-psutil \
  python3-requests \
  pciutils \
  curl \
  p7zip-full && \
  rm -rf /var/lib/apt/lists/*

# Hashcat - just for testing purposes
RUN cd /usr/local && \
    curl https://hashcat.net/files/hashcat-6.2.6.7z -o hashcat.7z && \
    7z x hashcat.7z && \
    ln -s /usr/local/hashcat-6.2.6/hashcat.bin /usr/local/bin/hashcat
    
# Hashtopolis
RUN mkdir -p /root/htpclient

WORKDIR /root/htpclient

RUN git clone https://github.com/s3inlc/hashtopolis-agent-python.git && \
  cd hashtopolis-agent-python && \
  ./build.sh && \
  mv hashtopolis.zip ../ && \
  cd ../ && rm -R hashtopolis-agent-python

ADD ./docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD []
